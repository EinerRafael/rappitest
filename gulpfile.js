var gulp = require("gulp");
var webserver = require('gulp-webserver');


gulp.task('webserver', function() {
  gulp.src('.')
    .pipe(webserver({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

gulp.task('production', function() {
    return gulp.src("src/**/*").pipe(gulp.dest("web/"));
});