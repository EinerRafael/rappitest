# Rappi Test

Esta es la solucion del problema [https://www.hackerrank.com/contests/101jan14/challenges/cube-summation]: 

Este Ejemplo esta escrito en:

  - Html5
  - CSS3
  - AngularJs

Para Correr:
  - Tener Instalado NodeJs y configurado npm en el PATH.
  - Clona este repositorio
  - Comando $cd RappiTest
  - Comando $npm install
  - Finalmente $gulp webserver


Abrir en el navegador: [http://localhost:8000/web/index.html]: 