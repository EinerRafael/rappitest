angular.module("RappiTest").controller("MainController", ['$scope', function($scope) {
    // Initial Values

    $scope.numberOfTest = 5;
    $scope.sizeMatrix = 10;
    $scope.hasInitialized = false;
    $scope.matrix = undefined;
    $scope.updateValues = {
        x: 0,
        y: 0,
        z: 0,
        value: 1,
        reset: function() {
            this.x = 0;
            this.y = 0;
            this.z = 0;
            this.value = 1;
        }
    };

    $scope.queryValues = {
        x1: 0,
        y1: 0,
        z1: 0,
        x2: 0,
        y2: 0,
        z2: 0
    };

    $scope.queries = [];

    $scope.initalizeMatrix = function() {
        if (validValues()) {
            $scope.matrix = new Matrix($scope.sizeMatrix);
            $scope.hasInitialized = true;
        }
        return false;
    }

    $scope.addValue = function() {
        var values = angular.copy($scope.updateValues);
        $scope.matrix.updateVal(values.x, values.y, values.z, values.value);
        $scope.updateValues.reset();
        return false;
    }

    $scope.hasQueies = function() {
        return ($scope.numberOfTest == 0);
    }

    $scope.query = function() {
        var values = angular.copy($scope.queryValues);
        var result = $scope.matrix.query(values.x1, values.y1, values.z1, values.x2, values.y2, values.z2);
        $scope.queries.push({
            x1: values.x1,
            y1: values.y1,
            z1: values.z1,
            x2: values.x2,
            y2: values.y2,
            z2: values.z2,
            result: result,
            date: new Date()
        });
        $scope.numberOfTest--;
        return false;
    }

    function validValues() {
        return true;
    }
}]);
