Matrix = function(_n) {
    var n = _n;
    var matrix = [];

    function initialize() {
        createMatrix();
    }

    function createMatrix() {
        for (var i = 0; i < n; i++) {
            matrix[i] = [];
            for (var j = 0; j < n; j++) {
                matrix[i][j] = [];
                for (var k = 0; k < n; k++) {
                    matrix[i][j][k] = 0;
                }
            }
        }
    }

    function updateVal(x, y, z, val) {
        matrix[x][y][z] = val;
    }

    this.updateVal = updateVal;

    function query(x1, y1, z1, x2, y2, z2) {
        var sum = 0;
        for (var i = x1; i < x2; i++) {
            for (var j = y1; j < y2; j++) {
                for (var k = z1; k < z2; k++) {
                    sum += matrix[i][j][k];
                }
            }
        }
        return sum;
    }

    this.query = query;

    function getSize() {
    	return n;
    }

    this.getSize = getSize;

    initialize();
}
